<?php

require __DIR__ . '/../vendor/autoload.php';

try {

    //Register an autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(array(
       '../app/controllers/',
       '../app/models/'
    ))->register();

    $di = require __DIR__ . '/../app/config/di.php';

    $application = new \Phalcon\Mvc\Application($di);
    echo $application->handle()->getContent();
} catch (\Phalcon\Exception $e) {
    echo "PhalconException: ", $e->getMessage();
}