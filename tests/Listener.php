<?php

class Listener extends PHPUnit_Framework_BaseTestListener {
    
    public function startTestSuite(PHPUnit_Framework_TestSuite $suite)
    {
        
        if ($suite->getName() === 'Db' || $suite->getName() === 'Functional') {
            $config = getDi()->get('config')->database;
            $file = __DIR__ . '/../extra/database/database.sql';
            $cmd = "mysql -h {$config->host} -u {$config->username} -p{$config->password} --init-command='DROP DATABASE IF EXISTS {$config->name}; CREATE DATABASE {$config->name}; USE {$config->name}' < $file";
            $output = array();
            $return = null;
            exec($cmd, $output, $return);
            if ($return !== 0) {
                die("Não foi possível concluir testes: " . join("\n", $output) . "\n");
            }
            //echo "Suite: " . $suite->getName() . " started\n";
        }
        
        
        
    }

}
