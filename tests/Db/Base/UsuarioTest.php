<?php

namespace Db\Base;

class UsuarioTest extends \PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        getDi()->get('db')->delete("usuarios");
    }

    public function invalidLogins()
    {
        return array(
           array(""),
           array("<leandro>"),
           array("abcde1fghij2asda"),
           array("Foo Bar"),
        );
    }

    /**
     * @dataProvider invalidLogins
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage Formato do login incorreto.
     */
    public function testCreateInvalidLogin($login)
    {

        # configure vars and stubs
        $di = getDi();
        $usuario = $di->get('Base\Usuario');
        $usuario->login = $login;
        $usuario->create();
    }

    public function invalidEmails()
    {
        return array(
           array(""),
           array("leandrolages"),
           array("leandrolages@gmail"),
           array("abcde1@fghij2asda.1"),
           array("username.@domain.com"),
           array("Foo.@Bar..com"),
           array("Foo@.var.com"),
        );
    }

    /**
     * @dataProvider invalidEmails
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage Formato do e-mail incorreto.
     */
    public function testCreateInvalidEmail($email)
    {

        # configure vars and stubs
        $di = getDi();
        $usuario = $di->get('Base\Usuario');
        $usuario->login = 'foobar';
        $usuario->email = $email;
        $usuario->create();
    }

    public function testCreate()
    {

        # configure vars and stubs
        $di = getDi();

        $di->set('now', function() {
            return '2015-01-01 00:00:01';
        });

        $usuario = $di->get('Base\Usuario');
        $usuario->login = 'foobar';
        $usuario->email = 'foo@bar.com';
        $this->assertTrue($usuario->create());

        $this->assertEquals('2015-01-01 00:00:01', $usuario->data_criacao);
        $this->assertGreaterThan(0, $usuario->id);
        $this->assertNull($usuario->senha);
        $this->assertNull($usuario->data_ultimo_login);
    }
    
    /**
     * @depends testCreate
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage Login já existente.
     */
    public function testCreateDuplicateLogin()
    {

        # configure vars and stubs
        $di = getDi();

        $di->set('now', function() {
            return '2015-01-01 00:00:01';
        });

        $usuario = $di->get('Base\Usuario');
        $usuario->login = 'foobar';
        $usuario->email = 'foo2@bar.com';
        $usuario->create();

    }
    
    /**
     * @depends testCreate
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage E-mail já existente.
     */
    public function testCreateDuplicateEmail()
    {

        # configure vars and stubs
        $di = getDi();

        $di->set('now', function() {
            return '2015-01-01 00:00:01';
        });

        $usuario = $di->get('Base\Usuario');
        $usuario->login = 'foo2bar';
        $usuario->email = 'foo@bar.com';
        $usuario->create();

    }
    
    /**
     * @depends testCreate
     * @expectedException \PDOException
     */
    public function testCreateDatabaseError()
    {

        # configure vars and stubs
        $di = getDi();

        $di->set('now', function() {
            return '2015-01-32 00:00:01';
        });

        $usuario = $di->get('Base\Usuario');
        $usuario->login = 'foobar100';
        $usuario->email = 'foo200@bar.com';
        $usuario->create();

    }

}
