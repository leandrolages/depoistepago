<?php

namespace Unit\Service;

class SignupTest extends \PHPUnit_Framework_TestCase
{

    public function testRun()
    {

        $login = 'foobar';
        $email = 'foo@bar.com';
        $expected = array('id' => 10, 'login' => $login, 'email' => $email, 'data_criacao' => '2015-01-01 12:45:17');
        $hash = 'q1w2e3';

        $diMock = $this->getMock('Phalcon\DI', array('get'));

        $componentCreateUsuarioMock = $this->getMockBuilder('Component\CreateUsuario')->disableOriginalConstructor()->getMock();
        $componentCreateUsuarioMock->expects($this->once())->method('withNoPassword')->with($login, $email)->willReturn($expected);
        $diMock->expects($this->at(0))->method('get')->with('Component\CreateUsuario')->willReturn($componentCreateUsuarioMock);
        
        $utilHashGeneratorMock = $this->getMockBuilder('Util\HashGenerator')->disableOriginalConstructor()->getMock();
        $utilHashGeneratorMock->expects($this->once())->method('get')->with($expected['id'], $expected['data_criacao'])->willReturn($hash);
        $diMock->expects($this->at(1))->method('get')->with('Util\HashGenerator')->willReturn($utilHashGeneratorMock);

        $componentMailWelcomeMock = $this->getMockBuilder('Component\Mail\Welcome')->disableOriginalConstructor()->getMock();
        $componentMailWelcomeMock->expects($this->once())->method('send')->with($login, $email, $hash);
        $diMock->expects($this->at(2))->method('get')->with('Component\Mail\Welcome')->willReturn($componentMailWelcomeMock);
        
        $service = new \Service\Signup;
        $service->setDI($diMock);

        $actual = $service->run($login, $email);

        $this->assertEquals($expected, $actual);
    }

}
