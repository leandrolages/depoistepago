<?php

namespace Unit\Util;

class HashGeneratorTest extends \PHPUnit_Framework_TestCase
{

    public function testGet()
    {

        $id = 10;
        $data = '2015-01-01 12:45:17';
        $seed = '123';
        $expected = 'ee1592e1dd27528b36302649da28563e';

        $diMock = $this->getMock('Phalcon\DI', array('get'));
        $diMock->expects($this->once())->method('get')->with('appSecret')->willReturn($seed);
        
        $util = new \Util\HashGenerator();
        $util->setDI($diMock);
        $actual = $util->get($id, $data);

        $this->assertEquals($expected, $actual);
    }

}
