<?php

namespace Unit\Component\Mail;

class SenderTest extends \PHPUnit_Framework_TestCase
{

    public function testSendHtml()
    {

        $to = 'foo@bar.com';
        $fromName = 'FooBar';
        $fromMail = 'from@mail.com';
        $subject = "Bem-vindo(a) ao depoistepago.com!";
        $html  = "Hello";

        $diMock = $this->getMock('Phalcon\DI', array('get'));
        
        $zendPartMock = $this->getMockBuilder('Zend\Mime\Part')->disableOriginalConstructor()->getMock();
        $zendPartMock->type = 'text/html';
        $diMock->expects($this->at(0))->method('get')->with('Zend\Mime\Part', array($html))->willReturn($zendPartMock);
        
        $bodyMock = $this->getMockBuilder('Zend\Mime\Message')->disableOriginalConstructor()->getMock();
        $bodyMock->expects($this->once())->method('setParts')->with(array($zendPartMock));
        $diMock->expects($this->at(1))->method('get')->with('Zend\Mime\Message')->willReturn($bodyMock);
        
        $zendMessageMock = $this->getMockBuilder('Zend\Mail\Message')->disableOriginalConstructor()->getMock();
        $zendMessageMock->expects($this->once())->method('setEncoding')->with('UTF-8')->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('addTo')->with($to)->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('addFrom')->with($fromMail, $fromName)->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('setSubject')->with($subject)->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('setBody')->with($bodyMock)->willReturnSelf();
        $diMock->expects($this->at(2))->method('get')->with('Zend\Mail\Message')->willReturn($zendMessageMock);
        
        $senderMock = $this->getMockBuilder('Zend\Mail\Transport\TransportInterface')->disableOriginalConstructor()->getMock();
        $senderMock->expects($this->once())->method('send')->with($zendMessageMock);
        $diMock->expects($this->at(3))->method('get')->with('mail')->willReturn($senderMock);
        
        $component = new \Component\Mail\Sender();
        $component->setDI($diMock);
        $component->sendHtml($to, $fromName, $fromMail, $subject, $html);
    }

}
