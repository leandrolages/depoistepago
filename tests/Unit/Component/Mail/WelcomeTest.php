<?php

namespace Unit\Component\Mail;

class WelcomeTest extends \PHPUnit_Framework_TestCase
{

    public function testSend()
    {

        $login = 'foobar';
        $email = 'foo@bar.com';
        $hash = 'q1w2e3';
        $html  = "Olá $login!<br><br>Seja bem-vindo(a) ao <strong>depoistepago</strong>.<br><br>Assim, suas despesas em grupo com seus amigos serão gerenciadas de uma forma inteligente.<br><br>";
        $html .= "Para validar sua conta, crie uma senha clicando <a href=\"http://test.depoistepago.com/validate/$hash\">aqui</a>.<br><br>";
        $html .= "Qualquer dúvida estamos a sua disposição!<br><br>Equipe depoistepago.";
        $subject = "Bem-vindo(a) ao depoistepago.com!";

        $diMock = $this->getMock('Phalcon\DI', array('get'));
        
        $config = new \Phalcon\Config(array(
           "setup" => array("baseDomain" => "test.depoistepago.com"),
           "mail"  => array("fromMail" => "contato@leandrolages.com", "fromName" => "depoistepago.com")
           )
        );
        $diMock->expects($this->at(0))->method('get')->with('config')->willReturn($config);
        
        $mailSenderMock = $this->getMockBuilder('Component\Mail\Sender')->disableOriginalConstructor()->getMock();
        $mailSenderMock->expects($this->once())->method('sendHtml')->with($email, "depoistepago.com", "contato@leandrolages.com", $subject, $html);
        /*$zendMessageMock->expects($this->once())->method('addTo')->with($email)->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('addFrom')->with("contato@leandrolages.com", "depoistepago.com")->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('setSubject')->with($subject)->willReturnSelf();
        $zendMessageMock->expects($this->once())->method('setBody')->with($html)->willReturnSelf();*/
        $diMock->expects($this->at(1))->method('get')->with('Component\Mail\Sender')->willReturn($mailSenderMock);
        
        
        /*$zendMailMock = $this->getMockBuilder('Zend\Mail\Transport\Smtp')->disableOriginalConstructor()->getMock();
        $zendMailMock->expects($this->once())->method('send')->with($zendMessageMock);
        $diMock->expects($this->at(2))->method('get')->with('mail')->willReturn($zendMailMock);*/

        $component = new \Component\Mail\Welcome();
        $component->setDI($diMock);
        $component->send($login, $email, $hash);
    }

}
