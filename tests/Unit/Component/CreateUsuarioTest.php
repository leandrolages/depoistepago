<?php

namespace Unit\Component;

class CreateUsuarioTest extends \PHPUnit_Framework_TestCase
{

    public function testWithNoPassword()
    {

        $login = 'foobar';
        $email = 'foo@bar.com';
        $expected = array('id' => 10, 'login' => $login, 'email' => $email);

        $diMock = $this->getMock('Phalcon\DI', array('get'));
        
        //$baseUsuario = $this->getMock('Base\Usuario', array('create', 'toArray'));
        $baseUsuarioMock = $this->getMockBuilder('Base\Usuario')->disableOriginalConstructor()->getMock();
        $baseUsuarioMock->expects($this->at(0))->method('create')->with(array('login' => $login, 'email' => $email))->willReturn(true);
        $baseUsuarioMock->expects($this->at(1))->method('toArray')->willReturn($expected);
        $diMock->expects($this->once())->method('get')->with('Base\Usuario')->willReturn($baseUsuarioMock);        
        
        $component = new \Component\CreateUsuario();
        $component->setDI($diMock);
        $actual = $component->withNoPassword($login, $email);

        $this->assertEquals($expected, $actual);
    }

}
