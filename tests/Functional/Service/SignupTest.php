<?php

namespace Functional\Service;

class SignupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage Formato do login incorreto.
     */
    public function testRunInvalidLogin() {
        $login = 'Leandro Lages';
        $email = 'leandrolages@gmail.com';
        $service = getDi()->get('Service\Signup');
        $service->run($login, $email);
    }
    
    /**
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage Formato do e-mail incorreto.
     */
    public function testRunInvalidEmail() {
        $login = 'leandrolages';
        $email = 'leandrolages@gmail';
        $service = getDi()->get('Service\Signup');
        $service->run($login, $email);
    }
    
    public function testRun()
    {

        $login = 'leandrolages';
        $email = 'leandrolages@gmail.com';

        $service = getDi()->get('Service\Signup');
        
        $user = $service->run($login, $email);

        $this->assertTrue($user['id'] > 0);
        $this->assertTrue($user['login'] === $login);
        $this->assertTrue($user['email'] === $email);
        $this->assertNotFalse(\DateTime::createFromFormat('Y-m-d H:i:s', $user['data_criacao']));
        $this->assertNull($user['senha']);
        $this->assertNull($user['data_ultimo_login']);
    }
    
    /**
     * @depends testRun
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage Login já existente.
     */
    public function testRunDuplicateLogin()
    {
        $login = 'leandrolages';
        $email = 'leandrolages@gmail.com';
        $service = getDi()->get('Service\Signup');
        $service->run($login, $email);
    }
    
    /**
     * @depends testRun
     * @expectedException \Phalcon\Exception
     * @expectedExceptionMessage E-mail já existente.
     */
    public function testRunDuplicateEmail()
    {
        $login = 'leandrol';
        $email = 'leandrolages@gmail.com';
        $service = getDi()->get('Service\Signup');
        $service->run($login, $email);
    }
    
}
