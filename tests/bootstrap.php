<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';

define('ROOT_PATH', __DIR__);
define('PATH_MODEL', __DIR__ . '/../app/models/');

set_include_path(
   ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// use the application autoloader to autoload the classes
// autoload the dependencies found in composer
$loader = new \Phalcon\Loader();
$loader->registerDirs(array(ROOT_PATH, PATH_MODEL));
$loader->register();

function getDi() {
    $env = 'test';
    return require __DIR__ . '/../app/config/di.php';
}