-- versão 1.0

CREATE
    TABLE usuarios
    (
        id BIGINT UNSIGNED AUTO_INCREMENT,
        login VARCHAR(60) NOT NULL,
        email VARCHAR(100) NOT NULL,
        senha VARCHAR(32) NULL,
        data_criacao DATETIME NOT NULL,
        data_ultimo_login DATETIME,
        PRIMARY KEY (id),
        CONSTRAINT UNIQUE (login),
        CONSTRAINT UNIQUE (email)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE
    TABLE grupos
    (
        id BIGINT UNSIGNED AUTO_INCREMENT,
        nome VARCHAR(60) NOT NULL,
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE
    TABLE usuarios_do_grupo
    (
        id BIGINT UNSIGNED AUTO_INCREMENT,
        id_usuario BIGINT UNSIGNED NOT NULL,
        id_grupo BIGINT UNSIGNED NOT NULL,
        PRIMARY KEY (id),
        CONSTRAINT UNIQUE (id_usuario, id_grupo)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE
    usuarios_do_grupo ADD CONSTRAINT FOREIGN KEY (id_usuario) REFERENCES usuarios (id) 
ON DELETE RESTRICT 
ON UPDATE RESTRICT;

ALTER TABLE
    usuarios_do_grupo ADD CONSTRAINT FOREIGN KEY (id_grupo) REFERENCES grupos (id) 
ON DELETE RESTRICT 
ON UPDATE RESTRICT;

CREATE   
    TABLE despesas
    (
        id BIGINT AUTO_INCREMENT,
        id_grupo BIGINT UNSIGNED NOT NULL,
        descricao VARCHAR(100) NOT NULL,
        periodo INTEGER NOT NULL,
        valor DECIMAL(10,2) NOT NULL,
        data_criacao DATETIME NOT NULL,
        data_ultima_edicao DATETIME NOT NULL,
        json_divisao TEXT,
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE
    despesas ADD CONSTRAINT FOREIGN KEY (id_grupo) REFERENCES grupos (id) 
ON DELETE RESTRICT 
ON UPDATE RESTRICT;
