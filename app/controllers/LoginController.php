<?php

class LoginController extends \Phalcon\Mvc\Controller
{

    public function indexAction() {
        
    }
    
    public function postAction()
    {
        try {
            
            $this->view->disable();
            
            $email = $this->request->getPost('email', 'email');
            $senha = $this->request->getPost('senha','string');
            
            $o = $this->di->get('Service\Usuario');
            $o->login($email, $senha);
            
            $data['status'] = 1;
            
        } catch (\Exception $e) {
            //echo "<xmp>"; die(print_r($e->getTraceAsString()));
            $data['status'] = 0;
            $data['error'] = $e->getMessage();
        }
        
        $response = $this->di->get('jsonResponse');
        $response->setContent(json_encode($data));
        $response->send();
        
    }

}
