<?php

class CreateAccountController extends \Phalcon\Mvc\Controller
{

    public function indexAction() {
        
    }
    
    public function postAction()
    {
        try {
            
            $this->view->disable();
            
            $nome = $this->request->getPost('nome', 'string');
            $email = $this->request->getPost('email', 'email');
            $senha = $this->request->getPost('senha');
            $grupo = $this->request->getPost('grupo','alphanum');
            
            $o = $this->di->get('Service\Usuario');
            $id = $o->criarConta($nome, $email, $senha, $grupo);
            
            $data['status'] = 1;
            $data['id'] = $id;
            
        } catch (\Exception $e) {
            //echo "<xmp>"; die(print_r($e->getTraceAsString()));
            $data['status'] = 0;
            $data['error'] = $e->getMessage();
        }
        
        $response = $this->di->get('jsonResponse');
        $response->setContent(json_encode($data));
        $response->send();
        
    }

}
