<?php

class TestController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        try {

            $this->view->disable();
            $email = 'leandro.faria@predicta.net';
            $idGrupo = 31;

            $o = $this->di->get('Service\UsuarioGrupo');
            $o->adicionarUsuarioNoGrupo($email, $idGrupo);

            $msg = "OK!";
            
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }
        
        $response = $this->di->get('jsonResponse');
        $response->setContent(json_encode($msg));
        $response->send();

    }
    
    public function divisaoAction()
    {
        try {

            $this->view->disable();

            $o = $this->di->get('Service\Despesa');
            $o->salvar(3, 31, "LUZ", 201411, 101.33, json_encode(array(103 => 101,104 => 0.33)));

            $msg = "OK!";
            
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }
        
        $response = $this->di->get('jsonResponse');
        $response->setContent(json_encode($msg));
        $response->send();

    }

}
