<?php

$router = new Phalcon\Mvc\Router();

# default route
$router->add("/", array(
    'controller' => 'index',
    'action' => 'index'
));

# not found route
$router->notFound(array(
    "controller" => "not-found",
    "action" => "index"
));

# custom routes
$router->add(
        "/como-funciona", array("controller" => "application", "action" => "comoFunciona")
);

$router->add(
        "/cadastrar", array("controller" => "index", "action" => "cadastrar")
);

$router->add(
        "/entrar", array("controller" => "index", "action" => "entrar")
);

$router->add(
        "/campeonatos", array("controller" => "application", "action" => "campeonatos")
);

$router->add(
        "/rodada", array("controller" => "application", "action" => "rodada")
);

$router->add("/jogo/:params", array(
    'controller' => 'jogo',
    'action' => 'index',
    'params' => 1
));

$router->add("/campeonato/:params", array(
    'controller' => 'application',
    'action' => 'campeonato',
    'params' => 1
));

return $router;
?>
