<?php

$di = new Phalcon\DI\FactoryDefault();

if (!isset($env)) {
    $env = getenv('APP_ENV') ? getenv('APP_ENV') : 'live';
}

$config = new Phalcon\Config\Adapter\Ini(__DIR__ . "/$env.ini");

$di->setShared('db', function() use ($config) {
    return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
       "host" => $config->database->host,
       "username" => $config->database->username,
       "password" => $config->database->password,
       "dbname" => $config->database->name,
       "port" => $config->database->port,
       "options" => array(
          PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
       )
    ));
});

$di->set('config', function() use ($config) {
    return $config;
});

$di->set('now', function() {
    return date('Y-m-d H:i:s');
});

$di->set('appSecret', function() {
    return 'jhajkdhas41235As1231234sdfqqwoipqewreop,!@#,,eqwe';
});

$di->set('randomPassword', function() {
    return mt_rand(12345678, 87654321);
});

$di->set('view', function() {
    $view = new \Phalcon\Mvc\View();
    $view->setViewsDir('../app/views/');
    return $view;
});

$di->set('mail', function() use ($config) {
    $mail = new Zend\Mail\Transport\Smtp();
    $cfg = array(
       'name' => $config->mail->name,
       'host' => $config->mail->host,
       'port' => $config->mail->port
    );
    if ($config->mail->auth) {
        $cfg = array_merge($cfg, array('connection_class' => 'login', 'connection_config' => array('username' => $config->mail->user, 'password' => $config->mail->password)));
    }
    $options = new Zend\Mail\Transport\SmtpOptions($cfg);
    $mail->setOptions($options);
    return $mail;
});

$di->set('jsonResponse', function() {
    $response = new Phalcon\Http\Response();
    $response->setContentType('text/x-json');
    $response->setHeader("Pragma", "no-cache");
    $response->setHeader("Cache-Control", "no-cache, must-revalidate");
    $response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . "GMT");
    $response->setExpires(new DateTime());
    return $response;
});

return $di;

