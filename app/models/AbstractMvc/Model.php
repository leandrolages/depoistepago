<?php

namespace AbstractMvc;

abstract class Model extends \Phalcon\Mvc\Model
{

    public function validate($validator)
    {

        parent::validate($validator);
        if ($this->validationHasFailed()) {
            $message = '';
            if ($validator->isSetOption('message')) {
                $message = $validator->getOption('message');
            }
            throw new \Phalcon\Exception($message);
        }

        return true;
    }

    public function create($data = null, $whiteList = null)
    {
        
        if (!parent::create($data, $whiteList)) {
            $message =  '';
            $messages = $this->getMessages();
            if (count($message) > 0) {
                $message = $messages[0];
            }
            throw new \Phalcon\Exception($message);
        }
        
        return true;
    }

}
