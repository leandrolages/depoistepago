<?php

namespace Base;

class Grupo extends \Phalcon\Mvc\Model
{

    protected function initialize()
    {
        $this->setSource("grupos");
        $this->hasMany("id", "Base\Despesa", "id_grupo");
    }

}

?>
