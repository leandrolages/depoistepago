<?php

namespace Base;

class UsuarioDoGrupo extends \Phalcon\Mvc\Model
{

    protected function initialize()
    {
        $this->setSource("usuarios_do_grupo");
        $this->belongsTo("id_usuario", "Base\Usuario", "id");
        $this->belongsTo("id_grupo", "Base\Grupo", "id");
    }
    
}

?>
