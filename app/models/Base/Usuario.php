<?php

namespace Base;

use Phalcon\Mvc\Model\Validator\Regex as RegexValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as Uniqueness;

class Usuario extends \AbstractMvc\Model
{

    protected function initialize()
    {
        $this->setSource("usuarios");
    }

    protected function beforeValidationOnCreate()
    {

        $this->validate(new RegexValidator(array('field' => 'login', 'pattern' => '/^[A-Za-z0-9_]{2,15}$/', 'message' => 'Formato do login incorreto.')));
        $this->validate(new \Validator\Email(array('field' => 'email', 'message' => 'Formato do e-mail incorreto.')));
        $this->validate(new Uniqueness(array('field' => 'login', 'message' => 'Login já existente.')));
        $this->validate(new Uniqueness(array('field' => 'email', 'message' => 'E-mail já existente.')));

        $this->data_criacao = $this->getDI()->get('now');
        $this->senha = NULL;
        $this->data_ultimo_login = NULL;
    }

}

?>
