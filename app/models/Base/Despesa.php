<?php

namespace Base;

class Despesa extends \Phalcon\Mvc\Model
{

    protected function initialize()
    {
        $this->setSource("despesas");
        $this->belongsTo("id_grupo", "Base\Grupo", "id", array('alias' => 'grupo'));
    }

}

?>
