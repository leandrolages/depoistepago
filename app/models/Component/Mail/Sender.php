<?php

namespace Component\Mail;

class Sender extends \Phalcon\Mvc\User\Component
{

    public function sendHtml($to, $fromName, $fromEmail, $subject, $html)
    {
        $part = $this->getDi()->get('Zend\Mime\Part', array($html));
        $part->type = 'text/html';

        $body = $this->getDi()->get('Zend\Mime\Message');
        $body->setParts(array($part));

        $message = $this->getDi()->get('Zend\Mail\Message');
        $message->setEncoding("UTF-8");
        $message->addTo($to)->addFrom($fromEmail, $fromName)->setSubject($subject)->setBody($body);

        $mail = $this->getDi()->get('mail');
        $mail->send($message);
    }

}
