<?php

namespace Component\Mail;

class Welcome extends \Phalcon\Mvc\User\Component
{

    const ENDPOINT = '/validate';
    const SUBJECT = 'Bem-vindo(a) ao depoistepago.com!';
    
    public function send($login, $email, $hash)
    {

        $config = $this->getDI()->get('config');
        
        $html  = "Olá $login!<br><br>Seja bem-vindo(a) ao <strong>depoistepago</strong>.<br><br>Assim, suas despesas em grupo com seus amigos serão gerenciadas de uma forma inteligente.<br><br>";
        $html .= "Para validar sua conta, crie uma senha clicando <a href=\"http://{$config->setup->baseDomain}" . self::ENDPOINT ."/$hash\">aqui</a>.<br><br>";
        $html .= "Qualquer dúvida estamos a sua disposição!<br><br>Equipe depoistepago.";
        
        $sender = $this->getDI()->get('Component\Mail\Sender');
        $sender->sendHtml($email, $config->mail->fromName, $config->mail->fromMail, self::SUBJECT, $html);
        
    }

}

?>
