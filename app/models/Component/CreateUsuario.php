<?php

namespace Component;

class CreateUsuario extends \Phalcon\Mvc\User\Component
{

    public function withNoPassword($login, $email)
    {
        $userModel = $this->getDI()->get('Base\Usuario');
        $userModel->create(array('login' => $login, 'email' => $email));
        return $userModel->toArray();
    }
}

?>
