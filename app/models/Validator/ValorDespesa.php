<?php

namespace Validator;

use Phalcon\Mvc\Model\Validator,
    Phalcon\Mvc\Model\ValidatorInterface;

class ValorDespesa extends Validator implements ValidatorInterface
{

    public function validate($model)
    {
        $field = $this->getOption('field');
        $value = $model->$field;

        if ($value <= 0) {
            $this->appendMessage("Despesa deve ser maior que zero", $field, "ValorDespesa");
            return false;
        }
        
        $jsonDecoded = json_decode($model->json_divisao,1);

        if (!is_array($jsonDecoded)) {
            $this->appendMessage("Divisão da despesa deve ser enviado", $field, "ValorDespesa");
            return false;
        }

        $divisao = array(); $soma = 0;
        foreach ($model->grupo->usuarios as $u) {
            if (isset($jsonDecoded[$u->id])) {
                $soma += ($divisao[$u->id] = $jsonDecoded[$u->id]);
            }
        }
        
        if ($soma != $value) {
            $this->appendMessage("A soma dos valores das despesas $soma deve igual $value.", $field, "ValorDespesa");
            return false;
        }
        
        $model->json_divisao = json_encode($divisao);

        return true;
    }

}