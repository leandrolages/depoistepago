<?php

namespace Validator;

use Phalcon\Mvc\Model\Validator,
    Phalcon\Mvc\Model\ValidatorInterface;

class Email extends Validator implements ValidatorInterface
{

    public function validate($model)
    {
        $field = $this->getOption('field');
        $email = $model->$field;

        $validator = $model->getDI()->get('Zend\Validator\EmailAddress');
        if (!$validator->isValid($email)) {
            $message = '';
            if ($this->isSetOption('message')) {
                $message = $this->getOption('message');
                $this->appendMessage($message);
            }
            return false;
        }

        return true;
    }

}
