<?php

namespace Validator;

use Phalcon\Mvc\Model\Validator,
    Phalcon\Mvc\Model\ValidatorInterface;

class Periodo extends Validator implements ValidatorInterface
{

    public function validate($model)
    {
        $field = $this->getOption('field');
        $value = $model->$field;

        if (strlen($value) != 6) {
            $this->appendMessage("Formato do período incorreto", $field, "Periodo");
            return false;
        }
        
        $ano = substr($value, 0,4);
        
        if ($ano < 2014 || $ano > 2030) {
            $this->appendMessage("Formato do ano do período incorreto", $field, "Periodo");
            return false;
        }
        
        $mes = (int) substr($value, 4);
        if ($mes < 1 || $mes > 12) {
            $this->appendMessage("Formato do mês do período incorreto", $field, "Periodo");
            return false;
        }
        
        
        return true;
    }

}