<?php

namespace Service;

class Signup extends \Phalcon\Mvc\User\Component
{
    
    public function run($login, $email)
    {
        $component = $this->getDI()->get('Component\CreateUsuario');
        $userData = $component->withNoPassword($login, $email);
        
        $hashComponent = $this->getDI()->get('Util\HashGenerator');
        $hash = $hashComponent->get($userData['id'], $userData['data_criacao']);
        
        $mailComponent = $this->getDI()->get('Component\Mail\Welcome');
        $mailComponent->send($login, $email, $hash);
        
        return $userData;
    }
}

?>
