<?php

namespace Util;

class HashGenerator extends \Phalcon\Mvc\User\Component
{

    public function get($id, $data)
    {
        $secret = $this->getDI()->get('appSecret');
        return md5($secret . '_' . $id . '_' . $data);
    }
}

?>
